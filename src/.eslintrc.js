// .eslintrc.js
module.exports = {
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint', 'react'],
    extends: ['plugin:@typescript-eslint/recommended', 'plugin:react/recommended'],
    rules: {
      // Add any custom ESLint rules here
    },
  };
  